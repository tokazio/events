package fr.tokazio.events;

import fr.tokazio.events.exceptions.AggregateNotFoundException;
import fr.tokazio.events.exceptions.BadCommandVersionException;
import fr.tokazio.events.impl.WeldJUnit4Runner;
import fr.tokazio.serializor.Serializor;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import java.io.File;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(WeldJUnit4Runner.class)
public class ArchitectureTest {

    private static final int TOT = 7100;//_000_000;

    @Inject
    private EventStore eventStore;

    @Inject
    private AggregateManager aggregateManager;

    @Inject
    private CommandDispatcher handler;

    private final Guid id = Guid.generate();

    @BeforeClass
    public static void setupOnce(){
        new File("mmaps/dummy.txt").delete();
    }

    @Before
    public void setup() throws IOException {
        System.out.println("EventStore is "+eventStore.getClass().getName());
        System.out.println("AggregateManager is "+aggregateManager.getClass().getName());
    }

    @Test
    public void test_create() {
        //given
        Command cmd = new DummyCreateCommand(id, 0);
        //when
        Aggregate aggregate = handler.dispatch(cmd);
        //then
        assertThat(aggregate).isNotNull();
    }

    @Test
    public void test_multi_create() {
        for (int i = 0; i < 1000; i++) {
            Command cmd = new DummyCreateCommand(Guid.generate(), 0);
            handler.dispatch(cmd);
        }
        //then
        assertThat(aggregateManager.size()).isEqualTo(1000);
    }

    @Test(expected = BadCommandVersionException.class)
    public void test_command_v2_not_handled() {
        //given
        Command cmd = new DummyCreateCommand(id, 0);
        handler.dispatch(cmd);
        cmd = new DummyUpdateTextCommandV2(id, 1, "V2");
        //when
        Aggregate aggregate = handler.dispatch(cmd);
    }

    @Test
    public void test_command_v2() {
        //given
        Command cmd = new DummyCreateCommand(id, 0);
        handler.dispatch(cmd);
        handler.register(new DummyUpdateTextCommandHandleV2(eventStore, aggregateManager));
        cmd = new DummyUpdateTextCommandV2(id, 1, "V2");
        //when
        DummyAggregate aggregate = handler.dispatch(cmd);
        //then
        assertThat(aggregate).isNotNull();
        assertThat(aggregate.getText()).isEqualTo("V2");
    }

    @Test//(timeout = 90000)
    public void test_update() {
        //given
        Command cmd = new DummyCreateCommand(id, 0);
        DummyAggregate aggregate = handler.dispatch(cmd);
        //when
        final long start = System.currentTimeMillis();
        for(int i = 0; i<TOT; i++) {
            cmd = new DummyUpdateTextCommand(id, aggregate.getVersion(), "change le texte " + i);
            aggregate = handler.dispatch(cmd);
        }
        final long t = System.currentTimeMillis()-start;
        System.out.println("Aggregate built with "+TOT+" events in "+t+"ms");
        //then
        assertThat(aggregate).isNotNull();
        assertThat(aggregate.getVersion()).isEqualTo(TOT + 1);
        assertThat(aggregate.getId()).isEqualTo(id);
        assertThat(aggregate.getText()).isEqualTo("change le texte " + (TOT - 1));
    }

    @Test(expected = AggregateNotFoundException.class)
    public void test_update_without_aggregate() {
        //given
        final Command cmd = new DummyUpdateTextCommand(id, 0, "change le texte ");
        //when
        final Aggregate aggregate = handler.dispatch(cmd);
    }


    @Test
    public void test_reload() throws IOException {
        //given
        /*
        final long start = System.currentTimeMillis();
        Command cmd = new DummyCreateCommand(id, 0);
        DummyAggregate aggregate = handler.dispatch(cmd);
        assertThat(aggregate).isNotNull();
        assertThat(aggregateManager.size()).isEqualTo(1);
        for(int i = 0;i<TOT;i++){
            cmd = new DummyUpdateTextCommand(id, 0,"change le texte " + i);
            handler.dispatch(cmd);
        }
        final long t = System.currentTimeMillis() - start;
        */
        //aggregateManager.reload();
        //when
        final long start2 = System.currentTimeMillis();
        DummyAggregate aggregate = aggregateManager.get(DummyAggregate.class, id);
        final long t2 = System.currentTimeMillis() - start2;
        //then
        assertThat(aggregate).isNotNull();
        assertThat(aggregate.getVersion()).isEqualTo(TOT + 1);
        assertThat(aggregate.getId()).isEqualTo(id);
        assertThat(aggregate.getText()).isEqualTo("change le texte "+(TOT-1));
    }
}
