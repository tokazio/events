package fr.tokazio.events;

import fr.tokazio.events.annotations.ImACommand;

@ImACommand
public class DummyUpdateTextCommandV2 extends AbstractCommand {

    private final String text;

    public DummyUpdateTextCommandV2(final Guid aggregateId, final long aggregateVersion, final String text) {
        super(aggregateId, aggregateVersion);
        this.text = text;
    }

    @Override
    public long getVersion() {
        return 2;
    }

    public String getText() {
        return text;
    }
}
