package fr.tokazio.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import fr.tokazio.events.annotations.ImAnEvent;
import fr.tokazio.serializor.Serialize;
import fr.tokazio.serializor.SerializeOrder;


@ImAnEvent
@Serialize
public class DummyTextUpdatedEvent extends AbstractEvent {

    @SerializeOrder(1)
    protected String text;

    @JsonCreator
    public DummyTextUpdatedEvent(@JsonProperty("aggregateId") final Guid aggregateId, @JsonProperty("aggregateVersion") final long aggregateVersion, @JsonProperty("text") final String text) {
        super(aggregateId, aggregateVersion);
        this.text = text;
    }

    /*
    public DummyTextUpdatedEvent(){
        super();
    }

    @Override
    public void serialize(ByteBuffer buffer){
        buffer.putInt(getClass().getName().length());
        buffer.put(getClass().getName().getBytes(Charset.forName("UTF-8")));

        buffer.putLong(getAggregateVersion());

        buffer.putInt(getAggregateId().toString().length());
        buffer.put(getAggregateId().toString().getBytes(Charset.forName("UTF-8")));

        buffer.putInt(text.length());
        buffer.put(text.getBytes(Charset.forName("UTF-8")));
    }

    @Override
    public void deserialize(ByteBuffer buffer){
        aggregateVersion = buffer.getLong();

        final int s = buffer.getInt();
        final byte[] b = new byte[s];
        buffer.get(b, 0, s);
        aggregateId = Guid.of(new String(b,Charset.forName("UTF-8")));

        final int s2 = buffer.getInt();
        final byte[] b2 = new byte[s2];
        buffer.get(b2, 0, s2);
        text = new String(b2,Charset.forName("UTF-8"));
    }
    */

    public String getText() {
        return this.text;
    }

    @Override
    public String toString() {
        String sb = "DummyTextUpdatedEvent{" + "aggregateId=" + getAggregateId() +
                ", text='" + text + '\'' +
                '}';
        return sb;
    }
}
