package fr.tokazio.events;

import fr.tokazio.events.annotations.ImACommand;

@ImACommand
public class DummyCreateCommand extends AbstractCommand {

    public DummyCreateCommand(Guid idAggregat, long aggregateVersion) {
        super(idAggregat, aggregateVersion);
    }
}
