package fr.tokazio.events;

import fr.tokazio.events.annotations.ImACommand;

@ImACommand
public class DummyUpdateTextCommand extends AbstractCommand {

    private final String text;

    public DummyUpdateTextCommand(final Guid aggregateId, final long aggregateVersion, final String text) {
        super(aggregateId, aggregateVersion);
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
