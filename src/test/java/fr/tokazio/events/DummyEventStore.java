package fr.tokazio.events;

import fr.tokazio.events.impl.FileEventStore;
import fr.tokazio.serializor.Serializor;

import javax.inject.Inject;
import java.io.File;
import java.nio.ByteBuffer;

public class DummyEventStore extends FileEventStore {

    static{
        new File("mmaps").mkdirs();
    }

    @Inject
    public DummyEventStore(Serializor<Event, ByteBuffer, Void> serializor) {
        super("mmaps/dummy.txt", serializor, 256*256);//not growing ?!
    }
}
