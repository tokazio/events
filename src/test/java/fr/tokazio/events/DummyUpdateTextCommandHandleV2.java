package fr.tokazio.events;


import java.io.IOException;

public class DummyUpdateTextCommandHandleV2 extends AbstractCommandExecutor<DummyAggregate, DummyUpdateTextCommandV2> {

    public DummyUpdateTextCommandHandleV2(final EventStore eventStore, final AggregateManager aggregateCache) {
        super(eventStore, aggregateCache);
    }

    @Override
    public DummyAggregate execute(final DummyUpdateTextCommandV2 command) {
        //reload
        DummyAggregate dummy = aggregateCache.get(DummyAggregate.class, command.getAggregateId());
        //dispatch
        Event evt = new DummyTextUpdatedEvent(command.getAggregateId(), command.getAggregateVersion(), command.getText());
        dummy = dummy.apply(evt);
        try {
            eventStore.put(evt);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dummy;
    }

    @Override
    public Class<DummyUpdateTextCommandV2> acceptCommandType() {
        return DummyUpdateTextCommandV2.class;
    }

    @Override
    public long acceptVersion() {
        return 2;
    }
}