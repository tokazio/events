package fr.tokazio.events;


import java.io.IOException;

public class DummyCreateCommandHandleV1 extends AbstractCommandExecutor<DummyAggregate, DummyCreateCommand> {


    public DummyCreateCommandHandleV1(final EventStore eventStore, final AggregateManager aggregateCache) {
        super(eventStore, aggregateCache);
    }

    public Class<DummyCreateCommand> acceptCommandType() {
        return DummyCreateCommand.class;
    }

    @Override
    public DummyAggregate execute(final DummyCreateCommand command) {
        //create
        DummyAggregate dummy = new DummyAggregate();
        dummy.id = command.getAggregateId();
        //dispatch
        Event evt = new DummyCreatedEvent(command.getAggregateId(), command.getAggregateVersion());
        dummy = dummy.apply(evt);
        try {
            eventStore.put(evt);
        } catch (IOException e) {
            e.printStackTrace();
        }
        aggregateCache.put(dummy);
        return dummy;
    }

    @Override
    public long acceptVersion() {
        return 1;
    }

}