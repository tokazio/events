package fr.tokazio.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import fr.tokazio.events.annotations.ImAnEvent;
import fr.tokazio.serializor.Serialize;

@ImAnEvent
@Serialize
public class DummyReopenedEvent extends AbstractEvent {

    @JsonCreator
    public DummyReopenedEvent(@JsonProperty("aggregateId") final Guid aggregateId, @JsonProperty("aggregateVersion") final long aggregateVersion) {
        super(aggregateId, aggregateVersion);
    }
}
