package fr.tokazio.events;

import javax.inject.Singleton;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class DummyAggregateManager implements AggregateManager {

    protected final Map<Guid,Aggregate> aggregates = new HashMap<>();

    @Override
    public <A extends Aggregate> A get(Class<A> clazz, Guid aggregateId) {
        return (A) aggregates.get(aggregateId);
    }

    @Override
    public void put(Aggregate aggregate) {
        aggregates.put(aggregate.getId(),aggregate);
    }

    @Override
    public long size() {
        return aggregates.size();
    }

    /*@Override
    public void clear() {
        aggregates.clear();
    }*/
}
