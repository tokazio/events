package fr.tokazio.events;


import java.io.IOException;

public class DummyUpdateTextCommandHandleV1 extends AbstractCommandExecutor<DummyAggregate, DummyUpdateTextCommand> {


    public DummyUpdateTextCommandHandleV1(final EventStore eventStore, final AggregateManager aggregateCache) {
        super(eventStore, aggregateCache);
    }

    @Override
    public DummyAggregate execute(final DummyUpdateTextCommand command) {
        //reload
        DummyAggregate dummy = aggregateCache.get(DummyAggregate.class, command.getAggregateId());
        //dispatch
        Event evt = new DummyTextUpdatedEvent(command.getAggregateId(), command.getAggregateVersion(), command.getText());
        dummy = dummy.apply(evt);
        try {
            eventStore.put(evt);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dummy;
    }

    @Override
    public Class<DummyUpdateTextCommand> acceptCommandType() {
        return DummyUpdateTextCommand.class;
    }

    @Override
    public long acceptVersion() {
        return 1;
    }
}