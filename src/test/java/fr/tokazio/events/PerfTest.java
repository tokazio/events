package fr.tokazio.events;

import fr.tokazio.events.impl.FileEventStore;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.util.Iterator;

public class PerfTest extends BuffersTest {

    //petit mac = 90 916 write in 1000ms
    //pc = 402 612 write in 1000ms
    @Ignore
    @Test
    public void test_perf_write() throws IOException {
        final int NB = 1_000_000;
        final long start = System.currentTimeMillis();
        int i = 0;
        while (System.currentTimeMillis() - start < 1000) {
            store.put(new DummyTextUpdatedEvent(Guid.generate(), i, "write #" + i));
            i++;
        }
        final long end = System.currentTimeMillis();
        System.out.println(i + " write in " + (end - start) + "ms");
    }

    //petit mac = 484 718 read in 1000ms
    //pc = 951 009 read in 1000ms
    @Ignore
    @Test
    public void test_perf_read() throws IOException {
        final Guid id = Guid.generate();
        int i = 0;
        while (i < 1_100_000) {
            store.put(new DummyTextUpdatedEvent(id, i, "write #" + i));
            i++;
        }
        store.close();
        System.out.println("Write ok");

        store = new FileEventStore(MMAPFOLDER + "/tests.txt", serializor);
        final Iterator<Event> it = store.iterator();
        final long start = System.currentTimeMillis();
        int j = 0;
        while (System.currentTimeMillis() - start < 1000 && it.hasNext()) {
            it.next();
            j++;
        }
        final long end = System.currentTimeMillis();
        System.out.println(j + " read in " + (end - start) + "ms");
    }
}
