package fr.tokazio.events;

import fr.tokazio.events.impl.*;
import fr.tokazio.serializor.Serializor;
import org.junit.*;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

//todo try accessing with multi thread
//todo try reading while writing
public class BuffersTest {

    protected static Serializor<Event, ByteBuffer, Void> serializor = new fr.tokazio.events.SerializorImpl<>();

    protected static final String MMAPFOLDER = "mmaps";

    protected EventStore store;

    @BeforeClass
    public static void setupOnce(){
        final File folder = new File(MMAPFOLDER);
        if(!folder.exists()) {
            System.out.println("Create " + MMAPFOLDER + " " + folder.mkdirs());
        }
        clean();
    }

    private static void clean() {
        final File mono = new File(MMAPFOLDER + "/mono.txt");
        if(mono.exists()) {
            System.out.println("Delete mono.txt " + mono.delete());
        }
        final File tests = new File(MMAPFOLDER + "/tests.txt");
        if(tests.exists()) {
            System.out.println("Delete tests.txt " + tests.delete());
        }
    }

    @Before
    public void setup() {
        store = new FileEventStore(MMAPFOLDER + "/tests.txt", serializor);
    }

    @After
    public void tearDown() throws IOException {
        store.close();
        clean();
    }

    @Test
    public void test_read_empty() {
        assertThat(store.size()).isEqualTo(0);
        final List<Event> events = store.all().collect(Collectors.toList());
        assertThat(events).isEmpty();
    }

    @Test
    public void test_read_one() throws IOException {
        store.put(new DummyCreatedEvent(Guid.generate(), 0));
        assertThat(store.size()).isEqualTo(1);
        final List<Event> events = store.all().collect(Collectors.toList());
        assertThat(events).hasSize(1);
    }

    @Test
    public void test_write() throws IOException {
        final Guid id = Guid.generate();
        store.put(new DummyCreatedEvent(id, 0));
        final List<Event> events = store.all().collect(Collectors.toList());
        assertThat(events).hasSize(1);
        assertThat(events.get(0).getAggregateId()).isEqualTo(id);
    }

    @Test
    public void test_append() throws IOException {
        final Guid id = Guid.generate();
        //Put one and close
        store = new FileEventStore(MMAPFOLDER + "/tests.txt", serializor);
        store.put(new DummyCreatedEvent(id, 0));
        assertThat(store.size()).isEqualTo(1);
        assertThat(store.count(id)).isEqualTo(1);
        store.close();
        //Put another and close
        store = new FileEventStore(MMAPFOLDER + "/tests.txt", serializor);
        store.put(new DummyCreatedEvent(id, 0));
        assertThat(store.size()).isEqualTo(2);
        assertThat(store.count(id)).isEqualTo(2);
        store.close();
        //Read
        store = new FileEventStore(MMAPFOLDER + "/tests.txt", serializor);
        assertThat(store.size()).isEqualTo(2);
        assertThat(store.count(id)).isEqualTo(2);
        final List<Event> events = store.all().collect(Collectors.toList());
        assertThat(events).hasSize(2);
    }

    //petit mac = 879ms pour 100 000 (en chargeant l'event)
    //win = Read via stream in 5611ms pour 1 000 000 avec le buffer en direct (pas de filter = pas de //)
    @Test
    public void test_stream_collect() throws IOException {
        final int NB = 1_000_000;
        for (int i = 0; i < NB; i++) {
            Guid id = Guid.generate();
            store.put(new DummyCreatedEvent(id, i));
            store.put(new DummyTextUpdatedEvent(id, i, "write #" + i));
        }
        store.close();

        store = new FileEventStore(MMAPFOLDER + "/tests.txt", serializor);
        final long start3 = System.currentTimeMillis();
        Stream<Event> stream = store.stream();
        List<Event> all = stream.parallel().collect(Collectors.toList());
        final long end3 = System.currentTimeMillis();
        System.out.println("Read via stream in " + (end3 - start3) + "ms");
        assertThat(all.size()).isEqualTo(NB * 2);
    }

    //petit mac = 1905ms pour 100 000 (en chargeant l'event)
    //win = 837ms pour 100 000 en utilisant directement le buffer
    //win = Read 1 000 000 via stream in 3108ms en //
    @Test
    public void test_stream_collect_type() throws IOException {
        final int NB = 1_000_000;
        for (int i = 0; i < NB; i++) {
            Guid id = Guid.generate();
            store.put(new DummyCreatedEvent(id, i));
            store.put(new DummyTextUpdatedEvent(id, i, "write #" + i));
        }
        store.close();

        store = new FileEventStore(MMAPFOLDER + "/tests.txt", serializor);
        final long start3 = System.currentTimeMillis();
        final Stream<DummyCreatedEvent> stream = store.stream().filter(new EventTypePredicate(DummyCreatedEvent.class));
        final List<DummyCreatedEvent> all = stream.parallel().collect(Collectors.toList());
        final long end3 = System.currentTimeMillis();
        System.out.println("Read " + all.size() + " via stream in " + (end3 - start3) + "ms");
        assertThat(all.size()).isEqualTo(NB);
    }

    //petit mac = 1908ms pour 100 000 (en chargeant l'event)
    //win = 765ms pour 100 000 en utilisant directement le buffer
    //win = Read 1 000 000 via stream in 4069ms en //
    @Test
    public void test_stream_collect_id() throws IOException {
        final Guid id = Guid.generate();
        final int NB = 1_000_000;
        for (int i = 0; i < NB; i++) {
            store.put(new DummyCreatedEvent(Guid.generate(), i));
            store.put(new DummyTextUpdatedEvent(id, i, "write #" + i));
        }
        store.close();

        store = new FileEventStore(MMAPFOLDER + "/tests.txt", serializor);
        final long start3 = System.currentTimeMillis();
        final Stream<Event> stream = store.stream().filter(new AggregateIdPredicate(id));
        final List<Event> all = stream.parallel().collect(Collectors.toList());
        final long end3 = System.currentTimeMillis();
        System.out.println("Read " + all.size() + " via stream in " + (end3 - start3) + "ms");
        assertThat(all.size()).isEqualTo(NB);
    }


    //win = Read 500000 via stream in 4402ms (8 proc)
    //petit mac = Read 500000 via stream in 47006 (2 proc)
    @Test
    public void test_stream_collect_type_then_id() throws IOException {
        final Guid id = Guid.generate();
        final int NB = 1_000_000;
        for (int i = 0; i < NB; i++) {
            store.put(new DummyCreatedEvent(Guid.generate(), i));
            store.put(new DummyTextUpdatedEvent(id, i, "write #" + i));
        }
        final Guid id2 = Guid.generate();
        for (int i = 0; i < NB / 2; i++) {
            store.put(new DummyTextUpdatedEvent(id2, i, "write #" + i));
        }
        store.close();

        store = new FileEventStore(MMAPFOLDER + "/tests.txt", serializor);
        final long start3 = System.currentTimeMillis();
        final Stream<Event> stream = store.stream().filter(new EventTypePredicate(DummyTextUpdatedEvent.class)).filter(new AggregateIdPredicate(id2));
        final List<Event> all = stream.parallel().collect(Collectors.toList());
        final long end3 = System.currentTimeMillis();
        System.out.println("Read " + all.size() + " via stream in " + (end3 - start3) + "ms");
        assertThat(all.size()).isEqualTo(NB / 2);
    }

    @Test
    public void test_reverse() throws IOException {
        final Guid id = Guid.generate();
        store.put(new DummyCreatedEvent(id, 0));
        final int NB = 100;
        for (int i = 0; i < NB; i++) {
            store.put(new DummyTextUpdatedEvent(id, i, "write #" + i));
        }
        store.put(new DummyValidatedEvent(id, NB + 1));
        //id2---
        final Guid id2 = Guid.generate();
        store.put(new DummyCreatedEvent(id2, 0));
        for (int j = 0; j < NB / 2; j++) {
            store.put(new DummyTextUpdatedEvent(id2, j, "write #" + j));
        }
        store.put(new DummyValidatedEvent(id2, NB / 2 + 1));
        //-------
        //Reopen id
        store.put(new DummyReopenedEvent(id, NB + 2));
        for (int k = 0; k < NB; k++) {
            store.put(new DummyTextUpdatedEvent(id, k + NB + 3, "write #" + k + NB + 3));
        }
        store.put(new DummyValidatedEvent(id, NB * 2 + 4));
        //end
        store.close();

        store = new FileEventStore(MMAPFOLDER + "/tests.txt", serializor);
        final long start3 = System.currentTimeMillis();
        final Stream<Event> stream = store.stream().sorted(new Reverse()).filter(new AggregateIdPredicate(id));
        final List<Event> all = stream.parallel().collect(Collectors.toList());
        final long end3 = System.currentTimeMillis();
        System.out.println("Read " + all.size() + " via stream in " + (end3 - start3) + "ms");
        assertThat(all.size()).isEqualTo(NB * 2 + 4);
        store.close();

        store = new FileEventStore(MMAPFOLDER + "/tests.txt", serializor);
        final long start4 = System.currentTimeMillis();
        final EventStream stream2 = (EventStream) store.stream().sorted(new Reverse()).filter(new AggregateIdPredicate(id));
        final Stream<Event> tw = stream2.takeWhile(EventStream.typeIsNot(DummyReopenedEvent.class));
        final List<Event> all2 = tw.parallel().collect(Collectors.toList());
        final long end4 = System.currentTimeMillis();
        System.out.println("Took " + all2.size() + " via stream in " + (end4 - start4) + "ms");
        assertThat(all2.size()).isEqualTo(NB + 1);
        store.close();
    }

    @Test
    public void test_grow() throws IOException {
        final Guid id = Guid.generate();
        //Put one and close
        store = new FileEventStore(MMAPFOLDER + "/tests.txt", serializor, 16);
        store.put(new DummyCreatedEvent(id, 0));
        assertThat(store.size()).isEqualTo(1);
        assertThat(store.count(id)).isEqualTo(1);
        store.close();
        //Put another and close
        store = new FileEventStore(MMAPFOLDER + "/tests.txt", serializor, 16);
        store.put(new DummyCreatedEvent(id, 0));
        assertThat(store.size()).isEqualTo(2);
        assertThat(store.count(id)).isEqualTo(2);
        store.close();
        //Read
        store = new FileEventStore(MMAPFOLDER + "/tests.txt", serializor, 16);
        assertThat(store.size()).isEqualTo(2);
        assertThat(store.count(id)).isEqualTo(2);
        final List<Event> events = store.all().collect(Collectors.toList());
        assertThat(events).hasSize(2);
    }
}

