package fr.tokazio.events;

import fr.tokazio.events.annotations.ImAnAggregate;

@ImAnAggregate
public class DummyAggregate extends AbstractAggregate {

    private String text;

    public DummyAggregate apply(final DummyCreatedEvent event) {
        this.id = event.getAggregateId();
        return this;
    }

    public DummyAggregate apply(final DummyTextUpdatedEvent event) {
        this.text = event.getText();
        return this;
    }

    public String getText() {
        return text;
    }


}
