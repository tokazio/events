package fr.tokazio.events;

import javax.inject.Inject;

public class DummyCommandHandler extends AbstractCommandDispatcher {

    @Inject
    public DummyCommandHandler(final EventStore eventStore, final AggregateManager aggregateCache) {
        super(eventStore, aggregateCache);
        register(new DummyCreateCommandHandleV1(eventStore, aggregateCache));
        register(new DummyUpdateTextCommandHandleV1(eventStore, aggregateCache));
    }


}
