package fr.tokazio.events.exceptions;

public class AggregateException extends RuntimeException {

    public AggregateException(final String s) {
        super(s);
    }

    public AggregateException(final Throwable t) {
        super(t);
    }
}
