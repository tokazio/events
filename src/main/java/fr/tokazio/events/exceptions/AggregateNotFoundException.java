package fr.tokazio.events.exceptions;

/**
 * @link https://github.com/mastoj/CQRSShop/blob/master/src/CQRSShop.Infrastructure/Exceptions/AggregateNotFoundException.cs
 */
public class AggregateNotFoundException extends RuntimeException {

    public AggregateNotFoundException(final String s) {
        super(s);
    }
}
