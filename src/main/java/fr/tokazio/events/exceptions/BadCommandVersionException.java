package fr.tokazio.events.exceptions;

public class BadCommandVersionException extends RuntimeException {

    public BadCommandVersionException(final String s) {
        super(s);
    }
}
