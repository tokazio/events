package fr.tokazio.events;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.tokazio.serializor.SerializeOrder;

public abstract class AbstractEvent implements Event {

    @SerializeOrder(-2)
    protected Guid aggregateId;
    @SerializeOrder(-1)
    protected long aggregateVersion;

    public AbstractEvent(final Guid aggregateId, final long aggregateVersion) {
        this.aggregateId = aggregateId;
        this.aggregateVersion = aggregateVersion;
    }

    public AbstractEvent(){

    }

    @Override
    public Guid getAggregateId() {
        return aggregateId;
    }

    @JsonIgnore
    @Override
    public long getAggregateVersion() {
        return aggregateVersion;
    }

    @JsonIgnore
    @Override
    public long getVersion() {
        return 1;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(getClass().getSimpleName()).append("{");
        sb.append("aggregateId=").append(aggregateId);
        sb.append(", aggregateVersion=").append(aggregateVersion);
        sb.append('}');
        return sb.toString();
    }
}
