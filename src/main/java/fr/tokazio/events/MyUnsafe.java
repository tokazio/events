package fr.tokazio.events;

import sun.misc.Unsafe;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class MyUnsafe {

    private static Unsafe theUnsafe;

    public MyUnsafe() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Constructor<Unsafe> unsafeConstructor = Unsafe.class.getDeclaredConstructor();
        unsafeConstructor.setAccessible(true);
        theUnsafe = unsafeConstructor.newInstance();
    }

    public Unsafe getUnsafe() {
        return theUnsafe;
    }

    public long allocateMemory(long size) { // C returns an address  | malloc()
        return theUnsafe.allocateMemory(size);
    }

    public long reallocateMemory(long address, long size) {// C  realloc()
        return theUnsafe.reallocateMemory(address, size);
    }

    public void freeMemory(long address) { // C free()
        theUnsafe.freeMemory(address);
    }

    public void setMemory(long address, long size, byte value) { // Cmemset()
        theUnsafe.setMemory(address, size, value);
    }

    public void zeroFill(long address, long size) {
        setMemory(address, size, (byte) 0);
    }

    public void copy(long fromAddress, long toAddress, long size) {// C memcpy()
        theUnsafe.copyMemory(fromAddress, toAddress, size);
    }

    public void throwChecked() {
        theUnsafe.throwException(new Exception());
    }
}
