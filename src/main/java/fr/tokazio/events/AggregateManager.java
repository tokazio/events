package fr.tokazio.events;

public interface AggregateManager {

    <A extends Aggregate> A get(final Class<A> clazz, final Guid aggregateId);

    void put(final Aggregate aggregate);

    long size();

    //void clear();
}
