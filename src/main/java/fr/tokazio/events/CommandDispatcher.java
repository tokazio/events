package fr.tokazio.events;

/**
 * A <code>CommandDispatcher</code> is responsible to receive one or more <code>Command</code> and dispatch it by a <code>CommandExecutor</code> that was previously registered.
 *
 * @see CommandExecutor
 * @see Aggregate
 * @see Command
 */
public interface CommandDispatcher {

    <T extends Aggregate, C extends Command> T dispatch(final C command);

    <A extends Aggregate, C extends Command> CommandDispatcher register(final CommandExecutor<A, C> commandHandle);

}
