package fr.tokazio.events;

import fr.tokazio.events.exceptions.BadCommandVersionException;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * This <code>CommandDispatcher</code> dispatch <code>Command</code> to its <code>CommandExecutor</code> previously registered.
 * It's linked to an <code>EventStore</code> to retrieve <code>Event</code> and a <code>AggregateManager</code> to maintain <code>Aggregate</code> state in memory (not rebuild the aggregate from scratch)
 *
 * @see EventStore
 * @see AggregateManager
 * @see Aggregate
 * @see Command
 */
public abstract class AbstractCommandDispatcher implements CommandDispatcher {

    protected final EventStore eventStore;
    protected final AggregateManager aggregateCache;

    private final List<CommandExecutor<Aggregate, Command>> commands = new ArrayList<>();

    @Inject
    public AbstractCommandDispatcher(final EventStore eventStore, final AggregateManager aggregateCache) {
        this.eventStore = eventStore;
        this.aggregateCache = aggregateCache;
    }

    @Override
    public <A extends Aggregate, C extends Command> A dispatch(final C command) {
        final StringBuilder sb = new StringBuilder();
        for (CommandExecutor<Aggregate, Command> commandHandle : commands) {
            sb.append("\tcommand ").append(commandHandle.acceptCommandType()).append(" @version ").append(commandHandle.acceptVersion()).append("\n");
            if (commandHandle.acceptCommandType().isAssignableFrom(command.getClass()) && commandHandle.acceptVersion() == command.getVersion()) {
                return (A) commandHandle.execute(command);
            }
        }
        throw new BadCommandVersionException(getClass().getName() + " can't dispatch " + command.getClass().getName() + " @version " + command.getVersion() + ".\nRegister an Handle for this Command in " + getClass().getName() + ".\nFor now, accepting only:\n" + sb.toString());
    }

    @Override
    public <A extends Aggregate, C extends Command> CommandDispatcher register(final CommandExecutor<A, C> commandHandle) {
        commands.add((CommandExecutor<Aggregate, Command>) commandHandle);
        return this;
    }
}
