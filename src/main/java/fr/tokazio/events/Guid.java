package fr.tokazio.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * Unique identifier base on java's <code>UUID</code>
 * For tests, this Guid can be forced to return always the same value with <code>alwaysReturn(value)</code>, this can be undone by <code>reset()</code>
 */
@JsonSerialize(using = ToStringSerializer.class, as = String.class)
public final class Guid implements Serializable, Comparable {

    private static String def;

    private final String val;

    private Guid(final String val) {
        this.val = val;
    }

    public static void alwaysReturn(final Guid def) {
        Guid.def = def.val;
    }

    public static void reset() {
        Guid.def = null;
    }

    public static Guid generate() {
        if (def == null) {
            return new Guid(UUID.randomUUID().toString());//todo maybe use a class to assure unique id by class
        }
        return new Guid(def);
    }

    @JsonCreator
    public static Guid of(@JsonProperty final String str) {
        return new Guid(UUID.fromString(str).toString());//Using UUID to validate input format
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Guid)) return false;
        Guid guid = (Guid) o;
        return Objects.equals(val, guid.val);
    }

    @Override
    public int hashCode() {
        return Objects.hash(val);
    }

    @Override
    public String toString() {
        return val;
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof Guid) {
            return o.toString().compareTo(toString());
        }
        return -1;
    }
}
