package fr.tokazio.events;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;

/**
 * Only an <code>Event</code> can mute an <code>Aggregate</code>.
 * An <code>Event</code> is created when a <code>Command</code> is handled to mute an <code>Aggregate</code>
 * An <code>Event</code> is versionned
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public interface Event extends Serializable {

    Guid getAggregateId();

    //Optimistic concurrency control OCC
    long getAggregateVersion();

    long getVersion();

}
