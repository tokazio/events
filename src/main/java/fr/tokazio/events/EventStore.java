package fr.tokazio.events;

import java.io.Closeable;
import java.io.IOException;
import java.util.Iterator;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * An <code>EventStore</code> is responsible to keep all <code>Event</code> that were produced by the system
 * It can easily retrieve all <code>Event</code> for a given <code>Aggregate</code>.
 */
public interface EventStore extends Closeable {

    Stream<Event> get(final Guid aggregateId);

    Stream<Event> all();

    void apply(final Guid aggregateId, Consumer<Event> action);

    void put(final Event... events) throws IOException;

    long count(final Guid id);

    long size();

    Iterator<Event> iterator();

    Stream<Event> stream();

    //void clear() throws IOException;
}
