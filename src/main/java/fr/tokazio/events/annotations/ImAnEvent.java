package fr.tokazio.events.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * An event is used to mute an aggregate.
 * A repeated list of same events gives the same aggregates.
 * It must implements Event and equals/hashCode.
 * It must be immutable.
 * Avoid setters.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface ImAnEvent {
}
