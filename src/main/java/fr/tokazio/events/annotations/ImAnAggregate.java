package fr.tokazio.events.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * An aggregate should extends <code>AbstractAggregate</code>
 * It must have a public no args constructor that should register transitions
 * It should have another private constructor that raiseEvent 'created' for this aggregate accedded by a static <code>create</code> method
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface ImAnAggregate {
}
