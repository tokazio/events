package fr.tokazio.events.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * A command is used to update the application.
 * It will validate its parameters then publish a event to mute an aggregate.
 * It is used via a command handler (extending <code>AbstractCommandDispatcher</code>)
 * It must implements Command.
 * It must be immutable.
 * Avoid setters.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface ImACommand {
}
