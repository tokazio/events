package fr.tokazio.events.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * A value object is immutable, this kind of object can't change and have no identity
 * <p>
 * You can use Lazy to compute values only once when needed like toString
 *
 * <code>
 * public class Point {
 * private final int x, y;
 * private final Lazy<String> lazyToString;
 * public Point(int x, int y) {
 * this.x = x;
 * this.y = y;
 * lazyToString = new Lazy<>();
 * }
 *
 * @Override public String toString() {
 * return lazyToString.getOrCompute( () -> "(" + x + ", " + y + ")");
 * }
 * }
 * </code>
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface ImAValueObject {
}
