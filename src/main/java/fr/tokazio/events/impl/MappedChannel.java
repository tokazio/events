package fr.tokazio.events.impl;

import fr.tokazio.events.Event;
import fr.tokazio.serializor.Serializor;
import org.apache.log4j.Logger;
import sun.nio.ch.DirectBuffer;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Iterator;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class MappedChannel {

    private static final Logger LOGGER = Logger.getLogger(MappedChannel.class);
    private final Serializor<Event, ByteBuffer, Void> serializor;
    private FileChannel channel;
    private MappedByteBuffer buffer;
    private final Object accessing = new Object();

    //header metadata
    static final int HEADER_LEN = Integer.BYTES + Integer.BYTES;
    private int count = 0;//number of element
    private int writePos = 0;//cursor write pos

    public MappedChannel(final Serializor<Event, ByteBuffer, Void> serializor) {
        this.serializor = serializor;
    }

    public static int nextPowerOf2(final int a) {
        int b = 1;
        while (b < a) {
            b = b << 1;
        }
        return b;
    }

    public MappedChannel open(final String filename, final long len) {
        LOGGER.debug("Opening " + filename + " as mapped file (" + len + ")...");
        final long l = computeSize(filename, len);
        try {
            channel = new RandomAccessFile(filename, "rw").getChannel();
            synchronized (accessing) {
                buffer = channel.map(FileChannel.MapMode.READ_WRITE, 0, l);
            }
            readHead();
        } catch (IOException ex) {
            throw new MappedChannelException(ex);
        }
        return this;
    }

    private void readHead() {
        synchronized (accessing) {
            buffer.rewind();
            count = buffer.getInt();
            writePos = buffer.getInt();
        }
        if (count == 0 && writePos == 0) {
            //this is a first read, need to create the header
            writeHead();
            writePos = HEADER_LEN;
        }
        goToWritePos();
    }

    private void writeHead() {
        synchronized (accessing) {
            final int currentPos = buffer.position();
            buffer.rewind();
            buffer.putInt(count);
            buffer.putInt(writePos);
            buffer.position(currentPos);
        }
    }

    //gives file length if exists, else given len
    private long computeSize(String filename, long len) {
        final File f = new File(filename);
        if (f.exists()) {
            return nextPowerOf2((int) f.length());
        }
        return len > 0 ? len : 1024;
    }

    //todo handle these writes in an atomic transaction
    public int write(final Event event) {
        if (!isClosed()) {
            synchronized (accessing) {
                growIfNeeded(event);
                serializor.serialize(event, buffer);
                count++;
                writePos = buffer.position();
            }
            writeHead();
        }
        return writePos;
    }

    public void flush() {
        if (!isClosed()) {
            synchronized (accessing) {
                buffer.force();
            }
        }
    }

    /**
     * Placing cursor at the write pos
     */
    private void goToWritePos() {
        synchronized (accessing) {
            buffer.position(writePos);
        }
    }

    public void close() throws IOException {
        if (!isClosed()) {
            synchronized (accessing) {
                flush();
                channel.close();
                ((DirectBuffer) buffer).cleaner().clean();//hack may not work on every platform
                buffer = null;//gc collectable
            }
        }
    }

    public boolean isClosed() {
        return buffer == null;
    }

    public Iterator<Event> iterator() {
        return new EventIterator(buffer.duplicate(), serializor);
    }

    public void forEach(Consumer<Event> action) {
        new EventStream<>(buffer.duplicate(), serializor).forEach(action);
    }

    public <E extends Event> Stream<E> stream() {
        return new EventStream<>(buffer.duplicate(), serializor);
    }

    public long count() {
        return count;
    }

    private void growIfNeeded(final Event event) {
        long addSize = serializor.size(event);
        if (addSize < 0) {
            throw new RuntimeException("Can't grow because serialized event size can't be found");//todo own exception
        }
        if (buffer.capacity() - buffer.position() < addSize) {
            flush();
            long newSize = buffer.capacity() * 2;
            while (newSize < addSize) {
                newSize = newSize * 2;
            }
            MappedByteBuffer oldBuffer = buffer;
            try {
                buffer = channel.map(FileChannel.MapMode.READ_WRITE, 0, newSize);
                ((DirectBuffer) oldBuffer).cleaner().clean();//hack may not work on every platform
                buffer.position(writePos);
            } catch (IOException ex) {
                throw new RuntimeException(ex);//todo own exception
            }
        }
    }
}
