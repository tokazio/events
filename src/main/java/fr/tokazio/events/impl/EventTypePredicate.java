package fr.tokazio.events.impl;

import fr.tokazio.events.Event;
import fr.tokazio.serializor.Utils;

import java.nio.ByteBuffer;
import java.util.function.Predicate;

public class EventTypePredicate<E extends Event> implements EventPredicate, Predicate<Event> {

    private final Class<E> clazz;

    public EventTypePredicate(final Class<E> clazz) {
        this.clazz = clazz;
    }

    @Override
    public boolean test(ByteBuffer buffer) {
        final String clazzname = Utils.fromString(buffer);
        return clazzname.equals(clazz.getName());
    }

    @Override
    public boolean test(Event o) {
        throw new UnsupportedOperationException("Not supported");
    }

}