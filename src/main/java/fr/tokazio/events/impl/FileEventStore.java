package fr.tokazio.events.impl;

import fr.tokazio.events.Event;
import fr.tokazio.events.EventStore;
import fr.tokazio.events.EventStoreException;
import fr.tokazio.events.Guid;
import fr.tokazio.serializor.Serializor;
import org.apache.log4j.Logger;

import javax.inject.Singleton;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.function.Consumer;
import java.util.stream.Stream;

@FileEngine
@Singleton
public class FileEventStore implements EventStore {

    private static final Logger LOGGER = Logger.getLogger(FileEventStore.class);

    private final String filename;
    private MappedChannel mappedChannel;
    private final Serializor<Event, ByteBuffer, Void> serializor;
    private final long length;

    public FileEventStore(final String filename, final Serializor<Event, ByteBuffer, Void> serializor, long length) {
        super();
        this.filename = filename;
        this.serializor = serializor;
        this.length = length;
        open();
    }

    private void open(){
        mappedChannel = new MappedChannel(serializor).open(filename, length);
        LOGGER.debug("Event store '" + filename + "' is opened with "+length+" bytes");
    }

    public FileEventStore(final String filename, final Serializor<Event, ByteBuffer, Void> serializor) {
        this(filename, serializor, 1024);
    }

    @Override
    public void put(final Event... events) {
        for (Event evt : events) {
            if (mappedChannel == null) {
                throw new EventStoreException("Not open");
            }
            LOGGER.debug("Writing '" + evt + "' to '" + filename + "'");
            mappedChannel.write(evt);
        }
    }

    @Override
    public long count(final Guid id) {
        return stream().count();
    }

    @Override
    public long size() {
        return mappedChannel.count();
    }

    @Override
    public Iterator<Event> iterator() {
        return mappedChannel.iterator();
    }

    @Override
    public Stream<Event> stream() {
        return mappedChannel.stream();
    }

    /*@Override
    public void clear() throws IOException {
        close();
        new File(filename).delete();
        open();
    }*/

    @Override
    public Stream<Event> get(final Guid aggregateId) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public Stream<Event> all() {
        return mappedChannel.stream();
    }

    @Override
    public void apply(final Guid aggregateId, final Consumer<Event> action) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public void close() throws IOException {
        if (mappedChannel != null) {
            mappedChannel.close();
        }
    }
}
