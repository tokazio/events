package fr.tokazio.events.impl;

import fr.tokazio.events.Event;
import fr.tokazio.events.EventStoreException;
import fr.tokazio.serializor.Serializor;
import fr.tokazio.serializor.Utils;

import java.nio.ByteBuffer;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

import static fr.tokazio.events.impl.EventStream.ActionType.*;
import static fr.tokazio.events.impl.MappedChannel.HEADER_LEN;

public class EventStream<E extends Event> implements Stream<E> {

    private static final int NB_PROC = Runtime.getRuntime().availableProcessors();

    private final ByteBuffer buffer;
    private final Serializor serializor;
    private final List<Action> actions = new ArrayList<>();
    private Collection<Integer> positions;
    private Runnable closeHandler;
    private boolean parallel;
    private boolean reverse;

    EventStream(final ByteBuffer buffer, final Serializor serializor) {
        this.serializor = serializor;
        this.buffer = buffer;
    }

    @Override
    public <R> Stream<R> map(Function<? super E, ? extends R> mapper) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public IntStream mapToInt(ToIntFunction<? super E> mapper) {
        throw new UnsupportedOperationException("Can't map to ints");
    }

    @Override
    public LongStream mapToLong(ToLongFunction<? super E> mapper) {
        throw new UnsupportedOperationException("Can't map to longs");
    }

    @Override
    public DoubleStream mapToDouble(ToDoubleFunction<? super E> mapper) {
        throw new UnsupportedOperationException("Can't map to doubles");
    }

    @Override
    public <R> Stream<R> flatMap(Function<? super E, ? extends Stream<? extends R>> mapper) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public IntStream flatMapToInt(Function<? super E, ? extends IntStream> mapper) {
        throw new UnsupportedOperationException("Can't flatMap to ints");
    }

    @Override
    public LongStream flatMapToLong(Function<? super E, ? extends LongStream> mapper) {
        throw new UnsupportedOperationException("Can't flatMap to longs");
    }

    @Override
    public DoubleStream flatMapToDouble(Function<? super E, ? extends DoubleStream> mapper) {
        throw new UnsupportedOperationException("Can't flatMap to doubles");
    }

    @Override
    public Stream<E> distinct() {
        //not used with an event store, events are unique then distinct is always true
        return this;
    }

    @Override
    public Stream<E> sorted() {
        //This event stream is naturally sorted
        return this;
    }

    public static EventPredicate typeIsNot(Class<? extends Event> clazz) {
        return buffer -> {
            final String clazzname = Utils.fromString(buffer);
            return !clazzname.equals(clazz.getName());
        };
    }

    @Override
    public Stream<E> sorted(Comparator<? super E> comparator) {
        if (!(comparator instanceof Reverse)) {
            throw new IllegalArgumentException("You can only use the Reverse comparator");
        }
        reverse = true;
        return this;
    }

    @Override
    public Stream<E> peek(Consumer<? super E> action) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    //see takeWhile in java9
    public Stream<E> takeWhile(EventPredicate predicate) {
        this.actions.add(new Action(TAKEWHILE, predicate));
        return this;
    }

    @Override
    public Stream<E> limit(long maxSize) {
        actions.add(new Action(LIMIT, maxSize));
        return this;
    }

    @Override
    public Stream<E> skip(long n) {
        actions.add(new Action(SKIP, n));
        return this;
    }

    @Override
    public void forEachOrdered(Consumer<? super E> action) {
        forEach(action);
    }

    @Override
    public Stream<E> filter(Predicate<? super E> predicate) {
        this.actions.add(new Action(FILTER, predicate));
        return this;
    }

    @Override
    public <A> A[] toArray(IntFunction<A[]> generator) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public E reduce(E identity, BinaryOperator<E> accumulator) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public Optional<E> reduce(BinaryOperator<E> accumulator) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public <U> U reduce(U identity, BiFunction<U, ? super E, U> accumulator, BinaryOperator<U> combiner) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public void forEach(Consumer<? super E> action) {
        for (Integer pos : applyActions()) {
            action.accept(load(pos));
        }
    }

    @Override
    public Stream<E> unordered() {
        reverse = false;
        return this;
    }

    @Override
    public Object[] toArray() {
        final List<Integer> filtered = applyActions();
        final Object[] arr = new Object[filtered.size()];
        int k = 0;
        for (Integer pos : filtered) {
            arr[k++] = load(pos);
        }
        return arr;
    }

    private E load(int pos) {
        buffer.position(pos);
        final String clazz = Utils.fromString(buffer);
        final E e = (E) serializor.deserialize(clazz, buffer);
        return e;
    }

    @Override
    public <R, A> R collect(Collector<? super E, A, R> collector) {
        final List<Integer> posList = applyActions();
        final long s = System.currentTimeMillis();
        A container = collector.supplier().get();
        for (Integer pos : posList) {
            collector.accumulator().accept(container, load(pos));
        }
        final long e = System.currentTimeMillis() - s;
        System.out.println("Collected " + posList.size() + " in " + e + "ms");
        return (R) container;

    }

    @Override
    public <R> R collect(Supplier<R> supplier, BiConsumer<R, ? super E> accumulator, BiConsumer<R, R> combiner) {
        final List<Integer> posList = applyActions();
        final long s = System.currentTimeMillis();
        R container = supplier.get();
        for (Integer pos : applyActions()) {
            accumulator.accept(container, load(pos));
        }
        final long e = System.currentTimeMillis() - s;
        System.out.println("Collected " + posList.size() + " in " + e + "ms");
        return container;
    }

    @Override
    public Optional<E> min(Comparator<? super E> comparator) {
        throw new UnsupportedOperationException("No min in an event store");
    }

    @Override
    public Optional<E> max(Comparator<? super E> comparator) {
        throw new UnsupportedOperationException("No max in an event store");
    }

    private void findPositions() {
        final long s = System.currentTimeMillis();
        positions = new FindPositions(buffer, reverse).positions();
        System.out.println("Found " + positions.size() + " positions in " + (System.currentTimeMillis() - s) + "ms");
    }

    @Override
    public boolean anyMatch(Predicate<? super E> predicate) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public boolean allMatch(Predicate<? super E> predicate) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public boolean noneMatch(Predicate<? super E> predicate) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public long count() {
        return applyActions().size();
    }

    @Override
    public Optional<E> findFirst() {
        final List<Integer> filtered = applyActions();
        if (!filtered.isEmpty()) {
            return Optional.of(load(filtered.get(0)));
        }
        return Optional.empty();
    }

    //todo see if using IntStream for positions can do it faster
    private List<Integer> applyActions() {
        findPositions();
        final long s = System.currentTimeMillis();
        List<Integer> filtered = new ArrayList<>(positions);
        for (Action action : actions) {
            switch (action.getType()) {
                case FILTER:
                    if (parallel && NB_PROC > 1) {
                        filtered = applyParallelFilter(filtered, action.getPredicate());
                    } else {
                        filtered = applyFilter(filtered, action.getPredicate());
                    }
                    break;
                case TAKEWHILE:
                    filtered = doTakeWhile(filtered, action.getPredicate());
                    break;
                case SKIP:
                    filtered = applySkip(filtered, action.getNumber());
                    break;
                case LIMIT:
                    filtered = applyLimit(filtered, action.getNumber());
                    break;
                default:
                    throw new RuntimeException("EventStream action not known");
            }
        }
        System.out.println("All action(s) gives " + filtered.size() + " element(s) in " + (System.currentTimeMillis() - s) + "ms with " + NB_PROC + " processor(s)");
        return filtered;
    }

    private List<Integer> applySkip(List<Integer> in, int nb) {
        return in.stream().skip(nb).collect(Collectors.toList());
    }

    private List<Integer> applyLimit(List<Integer> in, int nb) {
        return in.stream().limit(nb).collect(Collectors.toList());
    }

    private List<Integer> doTakeWhile(final List<Integer> in, final EventPredicate predicate) {
        final long sIn = System.currentTimeMillis();
        final List<Integer> keeps = new ArrayList<>();
        for (int k : in) {
            buffer.position(k);
            if (!predicate.test(buffer)) {
                break;
            }
            keeps.add(k);
        }
        System.out.println("\t* taken while '" + predicate.getClass().getSimpleName() + "' " + keeps.size() + " of " + in.size() + " in " + (System.currentTimeMillis() - sIn) + "ms");
        return keeps;
    }

    private List<Integer> applyFilter(final List<Integer> in, final EventPredicate predicate) {
        for (int k = in.size() - 1; k >= 0; k--) {
            buffer.position(in.get(k));
            if (!predicate.test(buffer)) {
                in.remove(k);
            }
        }
        return in;
    }

    @Override
    public Optional<E> findAny() {
        return findFirst();
    }

    @Override
    public Iterator<E> iterator() {
        return (Iterator<E>) new EventIterator(buffer.duplicate(), serializor);
    }

    @Override
    public Spliterator<E> spliterator() {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public boolean isParallel() {
        return parallel;
    }

    @Override
    public Stream<E> sequential() {
        parallel = false;
        return this;
    }

    //todo with spliterator
    @Override
    public Stream<E> parallel() {
        parallel = true;
        return this;
    }

    @Override
    public Stream<E> onClose(Runnable closeHandler) {
        this.closeHandler = closeHandler;
        return this;
    }

    @Override
    public void close() {
        closeHandler.run();
    }

    //-----------------------------------------------------------
    //-----------------------------------------------------------
    // statics
    //-----------------------------------------------------------
    //-----------------------------------------------------------

    private List<Integer> applyParallelFilter(final List<Integer> in, final EventPredicate predicate) {
        final long sIn = System.currentTimeMillis();
        final List<Integer> keeps = new ArrayList<>();
        final List<Do> dos = new ArrayList<>();
        int nb = in.size() / NB_PROC;
        int j = 0;
        for (int i = 0; i < NB_PROC; i++) {
            int end = j + nb;
            if (end > in.size()) {
                end = in.size();
            }
            dos.add(new Do(buffer.duplicate(), predicate, in.subList(j, end)));
            j += nb;
        }
        for (Do doit : dos) {
            doit.start();
        }
        for (Do doit : dos) {
            try {
                doit.join();
            } catch (InterruptedException e) {
                throw new EventStoreException(e);
            }
        }
        for (Do doit : dos) {
            keeps.addAll(doit.filtered());
        }
        System.out.println("\t* filtered '" + predicate.getClass().getSimpleName() + "' " + keeps.size() + " of " + in.size() + " in " + (System.currentTimeMillis() - sIn) + "ms");
        return keeps;
    }

    //-----------------------------------------------------------
    //-----------------------------------------------------------
    // inner classes
    //-----------------------------------------------------------
    //-----------------------------------------------------------

    private class Do extends Thread {

        private final List<Integer> filteredPositions;
        private final ByteBuffer buffer;
        private final EventPredicate predicate;

        private Do(ByteBuffer buffer, EventPredicate predicate, List<Integer> filtered) {
            this.buffer = buffer;
            this.predicate = predicate;
            this.filteredPositions = new ArrayList<>(filtered);
        }

        public List<Integer> filtered() {
            return filteredPositions;
        }

        public void run() {
            for (int k = filteredPositions.size() - 1; k >= 0; k--) {
                buffer.position(filteredPositions.get(k));
                if (!predicate.test(buffer)) {
                    filteredPositions.remove(k);
                }
            }
        }
    }

    enum ActionType {
        FILTER, TAKEWHILE, LIMIT, SKIP
    }

    private static class Action {

        private final ActionType type;
        private EventPredicate predicate;
        private int number;

        Action(final ActionType type, final Object predicate) {
            this.type = type;
            if (predicate instanceof EventPredicate) {
                this.predicate = (EventPredicate) predicate;
            } else {
                this.number = (int) predicate;
            }
        }

        public int getNumber() {
            return number;
        }

        public ActionType getType() {
            return this.type;
        }

        public EventPredicate getPredicate() {
            return this.predicate;
        }
    }

    private class FindPositions {

        private final ByteBuffer buffer;
        private final Deque<Integer> positions = new ArrayDeque<>();
        private int cursor;
        private boolean consumed;
        private final boolean reversed;

        //Find elements positions in the buffer and rewind
        public FindPositions(final ByteBuffer buffer, final boolean reversed) {
            this.buffer = buffer;
            this.reversed = reversed;
            buffer.position(HEADER_LEN);
            cursor = HEADER_LEN;
            while (hasNext()) {
                //go go go!
            }
            buffer.rewind();
        }

        public Deque<Integer> positions() {
            return this.positions;
        }

        private boolean hasNext() {
            if (consumed) {
                return false;
            }
            buffer.position(cursor);
            long objectSize = buffer.getLong();
            if (objectSize <= 0) {
                consumed = true;
                return false;
            }
            if (reversed) {
                positions.addFirst(buffer.position());
            } else {
                positions.addLast(buffer.position());
            }
            cursor = (int) (buffer.position() + objectSize);
            return true;
        }
    }
}