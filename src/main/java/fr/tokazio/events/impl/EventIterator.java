package fr.tokazio.events.impl;

import fr.tokazio.events.Event;
import fr.tokazio.serializor.Serializor;
import fr.tokazio.serializor.Utils;

import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.NoSuchElementException;

import static fr.tokazio.events.impl.MappedChannel.HEADER_LEN;

//todo iterator inheritance, 'next' method is similar for the 3 iterators

/**
 * Iterator over the mapped file to retrieve all events.
 */
public class EventIterator implements Iterator<Event> {

    private final ByteBuffer buffer;
    private final Serializor serializor;
    private int cursor;
    private boolean consumed;

    EventIterator(final ByteBuffer buffer, final Serializor serializor) {
        this.buffer = buffer;
        this.serializor = serializor;
        buffer.position(HEADER_LEN);
        cursor = HEADER_LEN;
    }

    @Override
    public synchronized boolean hasNext() {
        if (consumed) {
            return false;
        }
        buffer.position(cursor);
        long objectSize = buffer.getLong();
        if (objectSize <= 0) {
            consumed = true;
            return false;
        }
        return true;
    }

    @Override
    public synchronized Event next() {
        if (consumed) {
            throw new NoSuchElementException();
        }
        final String clazz = Utils.fromString(buffer);
        final Event e = (Event) serializor.deserialize(clazz, buffer);
        cursor = buffer.position();
        return e;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Can't remove an event from the store!");
    }

}