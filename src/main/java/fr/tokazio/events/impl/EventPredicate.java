package fr.tokazio.events.impl;

import java.nio.ByteBuffer;

@FunctionalInterface
public interface EventPredicate {

    boolean test(ByteBuffer buffer);
}
