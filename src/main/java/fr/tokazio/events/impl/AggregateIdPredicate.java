package fr.tokazio.events.impl;

import fr.tokazio.events.Event;
import fr.tokazio.events.Guid;
import fr.tokazio.serializor.Utils;

import java.nio.ByteBuffer;
import java.util.function.Predicate;

public class AggregateIdPredicate implements EventPredicate, Predicate<Event> {

    private final Guid id;

    public AggregateIdPredicate(Guid id) {
        this.id = id;
    }

    @Override
    public boolean test(ByteBuffer buffer) {
        int skip = buffer.getInt();//get classname length
        buffer.position(buffer.position() + skip + Long.BYTES);//skip it and skip version
        final String id = Utils.fromString(buffer);
        return id.equals(this.id.toString());
    }

    @Override
    public boolean test(Event o) {
        throw new UnsupportedOperationException("Not supported");
    }
}
