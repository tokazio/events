package fr.tokazio.events.impl;


public class MappedChannelException extends RuntimeException {

    public MappedChannelException(Throwable ex) {
        super(ex);
    }
}
