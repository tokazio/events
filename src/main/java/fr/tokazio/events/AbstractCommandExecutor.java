package fr.tokazio.events;

import javax.inject.Inject;

/**
 * This <code>CommandExecutor</code> is linked to an <code>EventStore</code> to retrieve <code>Event</code> and to a <code>AggregateManager</code> to maintain <code>Aggregate</code> state in memory
 *
 * @param <A> <code>Aggregate</code> type returned by the <code>Command</code> handling
 * @param <C> <code>Command</code> type handled
 * @see CommandExecutor
 * @see Event
 * @see EventStore
 * @see Aggregate
 * @see AggregateManager
 */
public abstract class AbstractCommandExecutor<A extends Aggregate, C extends Command> implements CommandExecutor<A, C> {

    protected final EventStore eventStore;
    protected final AggregateManager aggregateCache;

    @Inject
    public AbstractCommandExecutor(final EventStore eventStore, final AggregateManager aggregateCache) {
        this.eventStore = eventStore;
        this.aggregateCache = aggregateCache;
    }

}
