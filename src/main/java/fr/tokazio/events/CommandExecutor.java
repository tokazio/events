package fr.tokazio.events;

/**
 * Handle a <code>Command</code> by its type and version
 *
 * @param <A> <code>Aggregate</code> type returned after the <code>Command</code> was handled
 * @param <C> <code>Command</code> type to dispatch
 * @see Aggregate
 * @see Command
 */
public interface CommandExecutor<A extends Aggregate, C extends Command> {

    A execute(final C command);

    Class<C> acceptCommandType();

    long acceptVersion();

}
