package fr.tokazio.events;

import java.util.List;
import java.util.stream.Stream;

/**
 * Must have empty constructor to be created by reflection.
 * Then apply events to mutate.
 */
public interface Aggregate {

    Guid getId();

    String getIdAsString();

    <T extends Aggregate> T apply(final Event... events);

    <T extends Aggregate> T apply(final List<Event> events);

    <T extends Aggregate> T apply(final Stream<Event> events);

    long getVersion();//todo optimistic concurrency control OCC when putting a new event, the version must be the same has loaded before adding the new event
}
