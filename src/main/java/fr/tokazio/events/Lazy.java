package fr.tokazio.events;

import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;

/**
 * @see remy forax beautyfull logger (devoxx paris 2018) with java invoke technics to avoid volatile (volatile forcing ram re-read of all vars!)
 */
public final class Lazy<T> {

    private volatile T value;

    public T getOrCompute(Supplier<T> supplier) {
        final T result = value; // Just one volatile read 
        return result == null ? maybeCompute(supplier) : result;
    }

    private synchronized T maybeCompute(Supplier<T> supplier) {
        if (value == null) {
            value = requireNonNull(supplier.get());
        }
        return value;
    }
}