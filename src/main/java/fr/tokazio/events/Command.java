package fr.tokazio.events;

/**
 * A <code>Command</code> is responsible to change the system state.
 * A <code>CommandDispatcher</code> will dispatch it via a <code>CommandExecutor</code> to validate it against a <code>Aggregate</code> state.
 * If a <code>Command</code> is successfully handled, it may produce an <code>Event</code> that will update the <code>Aggregate</code> state.
 * A <code>Command</code> is versioned and can be handled only by the same versioned <code>CommandExecutor</code>
 *
 * @see CommandDispatcher
 * @see CommandExecutor
 * @see Aggregate
 * @see Event
 */
public interface Command {

    Guid getAggregateId();

    //Optimistic concurrency control OCC
    long getAggregateVersion();

    long getVersion();

}
