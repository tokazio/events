package fr.tokazio.events;

public class EventStoreException extends RuntimeException {

    public EventStoreException(String text) {
        super(text);
    }

    public EventStoreException(Throwable ex) {
        super(ex);
    }
}
