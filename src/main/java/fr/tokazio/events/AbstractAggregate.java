package fr.tokazio.events;

import fr.tokazio.events.exceptions.AggregateException;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Stream;

/**
 * This <code>Aggregate</code> maintain a <code>Guid</code> and a version.
 * It's responsible to apply events to mute its state.
 */
public abstract class AbstractAggregate implements Aggregate {

    private static final MethodHandles.Lookup publicLookup = MethodHandles.publicLookup();
    public static final String CAN_T_APPLY_NULL_EVENT = "Can't apply null event";
    private final Map<String, MethodHandle> handles = new TreeMap<>();

    protected Guid id;
    private long version = 0;

    @Override
    public <T extends Aggregate> T apply(final Event... events) {
        if (events == null) {
            throw new AggregateException(CAN_T_APPLY_NULL_EVENT);
        }
        return apply(Arrays.stream(events));
    }

    @Override
    public <T extends Aggregate> T apply(final List<Event> events) {
        if (events == null) {
            throw new AggregateException(CAN_T_APPLY_NULL_EVENT);
        }
        return apply(events.stream());
    }

    @Override
    public <T extends Aggregate> T apply(final Stream<Event> events) {
        if (events == null) {
            throw new AggregateException(CAN_T_APPLY_NULL_EVENT);
        }
        events.forEach(event -> {
            //@see optimistic concurrency control OCC or MVCC
            //if (event.getAggregateVersion() != 0 && version != event.getAggregateVersion()) {
            //   throw new AggregateException("Expected version " + event.getAggregateVersion() + " found " + version);
            //}
            final MethodHandle apply = handles.computeIfAbsent(event.getClass().getName(), k -> {
                try {
                    return publicLookup.findVirtual(this.getClass(), "apply", MethodType.methodType(this.getClass(), event.getClass()));
                } catch (NoSuchMethodException | IllegalAccessException e) {
                    throw new AggregateException(e);
                }
            });
            try {
                apply.invoke(this, event);
                version++;
            } catch (Throwable throwable) {
                throw new AggregateException(throwable);
            }
        });
        return (T) this;
    }

    @Override
    public long getVersion() {
        return version;
    }

    @Override
    public Guid getId() {
        return id;
    }

    @Override
    public String getIdAsString() {
        return getId().toString();
    }
}
