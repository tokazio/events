package fr.tokazio.events;

import javax.inject.Inject;

public abstract class AbstractCommand implements Command {

    private final Guid idAggregat;
    private final long aggregateVersion;

    @Inject
    public AbstractCommand(final Guid idAggregat, final long aggregateVersion) {
        this.idAggregat = idAggregat;
        this.aggregateVersion = aggregateVersion;
    }

    @Override
    public Guid getAggregateId() {
        return idAggregat;
    }

    @Override
    public long getAggregateVersion() {
        return aggregateVersion;
    }

    @Override
    public long getVersion() {
        return 1;
    }

}
